#+TITLE: Guide pour la rédaction du rapport de TP d'analyse de données
#+SUBTITLE: M1 ASA, Université de Bordeaux, Année 2023--2024
#+AUTHOR: Frédéric Santos, Antoine Souron
#+DATE: \today
#+EMAIL: frederic.santos@u-bordeaux.fr
#+STARTUP: showall num
#+OPTIONS: email:t toc:nil ^:nil
#+LATEX_HEADER: \usepackage[natbibapa]{apacite}
#+LATEX_HEADER: \usepackage[french]{babel}
#+LATEX_HEADER: \usepackage{a4wide}
#+LATEX_HEADER: \usepackage{mathpazo}
#+LATEX_HEADER: \usepackage{booktabs}
#+LATEX_HEADER: \usepackage{pmboxdraw}
#+LATEX_HEADER: \usepackage[matha,mathb]{mathabx}
#+LATEX_HEADER: \usepackage{amsmath}
#+LATEX_HEADER: \usepackage{titlesec}
#+LATEX_HEADER: \titlelabel{\thetitle.\quad}
#+LATEX_HEADER: \usepackage[usenames,dvipsnames]{xcolor} % For colors with friendly names
#+LATEX_HEADER: \usepackage{minted}
#+LATEX_HEADER: \usepackage{mdframed}                    % Companion of minted for code blocks
#+LATEX_HEADER: \usepackage{fancyvrb}                    % For verbatim R outputs
#+LATEX_HEADER: \usemintedstyle{friendly} % set style if needed, see https://frama.link/jfRr8Lpj
#+LATEX_HEADER: \mdfdefinestyle{mystyle}{linecolor=gray!30,backgroundcolor=gray!30}
#+LATEX_HEADER: \BeforeBeginEnvironment{minted}{%
#+LATEX_HEADER: \begin{mdframed}[style=mystyle]}
#+LATEX_HEADER: \AfterEndEnvironment{minted}{%
#+LATEX_HEADER: \end{mdframed}}
#+LATEX_HEADER: %% Formatting of verbatim outputs (i.e., outputs of R results):
#+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{Verbatim}{%
#+LATEX_HEADER:   fontsize = \small,
#+LATEX_HEADER:   %frame = leftline,
#+LATEX_HEADER:   formatcom = {\color{gray!97}}
#+LATEX_HEADER: }
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \usepackage{url}
#+LATEX_HEADER: %% For DOI hyperlinks in biblio:
#+LATEX_HEADER: \usepackage{doi}
#+LATEX_HEADER: \renewcommand{\doiprefix}{}
#+PROPERTY: header-args :tangle yes
#+BIBLIOGRAPHY: ~/PACEA_MyCore/complete_biblio.bib
#+CITE_EXPORT: natbib apacite
#+LANGUAGE: fr

* Consignes générales
- Le rapport sera à remettre jusqu'au 19/01/2024, par mail à Antoine Souron et Frédéric Santos (=<antoine.souron@u-bordeaux.fr>, <frederic.santos@u-bordeaux.fr>=).
- Format : le rapport lui-même doit être un fichier PDF d'une dizaine de pages maximum (figures incluses).
- Transmettre également les données brutes (TPS) et votre code source R, l'idéal étant de compresser la totalité de votre répertoire de travail (tel qu'exposé en Section [[Structurer votre répertoire de travail]]), et de transmettre le =.zip= (ou autres formats, =.7z=, =.tar.gz=, etc.) par courriel.

* Acquisition des données avec tpsDig2
Au moins un des étudiants de chaque groupe devra faire une seconde prise de landmarks sur les figurines, pour pouvoir par la suite évaluer l'erreur intra-observateur (entre les deux séries de l'étudiant courageux) et l'erreur inter-observateurs (entre les différents étudiants).

Prenons le cas fictif d'un groupe de deux étudiants, nommés Jack et Oggy. Supposons que Jack a effectué deux séries de mesures (pour l'étude de l'erreur intra-observateurs), et Oggy en a effectué une seule (on utilisera donc cette série de mesures, et l'une des deux séries de Jack, pour l'étude de l'erreur inter-observateurs)[fn::Évidemment, vous êtes libres d'imaginer des plans d'étude plus complexes.].

Ce groupe de deux étudiants exportera alors trois fichiers =.TPS=, nommés (par exemple) =figures_jack1.TPS=, =figures_jack2.TPS= et =figures_oggy.TPS=. (En résumé, pour chaque étudiant, on exporte donc un fichier TPS par série de mesures.) Les fichiers =.TPS= que vous obtiendrez devront en tous points ressembler à [[https://gitlab.com/f-santos/tp-master-1-bgs/-/blob/master/data/figures.TPS][celui utilisé lors du TP]]. En particulier :

1. Veillez à (re)nommer vos fichiers =.jpg= de manière standardisée, avec une nomenclature précise. Les figures mesurées par Jack pour sa première série de mesures devront avoir pour noms =Aline_1_07_Jack1.jpg=, =Armance_2_08_Jack1.jpg=, etc. Bien entendu, les figures mesurées par Jack pour sa seconde série de mesures devront avoir pour noms =Aline_1_07_Jack2.jpg=, =Armance_2_08_Jack2.jpg=, etc. ; et les figures mesurées par Oggy pour son unique série de mesures devront avoir pour noms =Aline_1_07_Oggy.jpg=, =Armance_2_08_Oggy.jpg=, etc.
2. Attention aux caractères spéciaux ("é", ...) dans vos fichiers TPS. Évitez-les si possible ; il n'est pas garanti qu'ils soient correctement lus par R.
3. Dans vos fichiers TPS, utilisez le point comme séparateur décimal, et non la virgule. Vos nombres devront donc être indiqués "25.5" et non "25,5".
4. Veillez à ce que /toutes/ les figurines présentes dans votre fichier TPS aient bien 15 landmarks. Dans le cas contraire, vous obtiendrez le message d'erreur suivant :
   #+begin_example
Erreur dans geomorph::readland.tps(file = file, specID = specID) : 
Different numbers of landmarks among specimens
   #+end_example

* Travailler efficacement avec R
Les étapes suivantes sont /fortement recommandées/ pour faciliter votre travail d'analyse de données avec R.

** Créer un projet Rstudio
À l'aide du menu =File > New Project= de Rstudio, créer un projet de type =New Directory= quelque part sur votre ordinateur. Cela créera un dossier à l'emplacement que vous aurez choisi, et ce dossier contiendra un fichier projet =.Rproj=.

C'est ce fichier projet qu'il faudra ouvrir au début de chacune de vos sessions de travail : en double-cliquant sur ce fichier depuis votre explorateur de fichiers, Rstudio s'ouvre en fixant aussitôt pour vous le répertoire courant de la session, et d'autres paramètres utiles que vous pouvez sauvegarder d'une session de travail à l'autre.

Pour en savoir plus sur l'utilité de travailler "en mode projet", vous pourrez par exemple consulter :
#+begin_center
\url{https://www.book.utilitr.org/03_fiches_thematiques/fiche_rprojects}
#+end_center

** Structurer votre répertoire de travail
Pour vous retrouver plus facilement dans le répertoire créé à l'étape précédente, on peut suggérer de suivre l'organisation suivante :

#+begin_src bash :results output :exports results :cache yes
cd ~/PACEA_MyCore/Cours_M1_BGS/current/rapport_M1_asa
tree
#+end_src

#+RESULTS[44deb19d054038b7389fa5ac26086109ae06b01e]:
#+begin_example
.
├── analyses.R
├── data
│   ├── figures_jack1.TPS
│   ├── figures_jack2.TPS
│   └── figures_oggy.TPS
├── figures
│   ├── acp.png
│   └── boxplots_M2.png
├── rapport_jack_oggy.odt
├── rapport_jack_oggy.pdf
├── rapport_M1_asa.Rproj
└── tables
    └── table_concordance.csv
#+end_example

Plus en détail :
- créer un dossier =data= où vous placerez toutes les données brutes utiles pour la rédaction de votre rapport ;
- enregistrer dans un dossier =figures= toutes les figures enregistrées sous Rstudio que vous jugerez utile d'inclure dans votre rapport final ;
- idem (éventuellement) avec un dossier =tables= pour les tables (au format CSV) exportées durant vos analyses ;
- votre script R (ici nommé =analyses.R=), ainsi que votre rapport lui-même, peuvent se situer à la racine de votre répertoire.

* Analyses statistiques

** Trame suggérée
Vous pourrez vous inspirer de la trame d'analyse suivante, mais il ne s'agit pas nécessairement d'une trame à suivre à la lettre. Vous pouvez opter pour une démarche différente, et justifier l'intérêt de chaque analyse ou résultat présenté dans votre rapport.

1. Importer les fichiers TPS des 2 ou 3 étudiants du groupe, vérifier l'absence d'erreurs grossières ou de problèmes d'importation.
2. Évaluation de l'erreur intra ou inter-observateurs, en considérant les variables isolément (voir [[https://f-santos.gitlab.io/tp-master-1-bgs/%C3%A9valuation-des-erreurs-de-mesure.html#un-exemple-d%C3%A9valuation-de-lerreur-inter-observateur-sur-une-variable-donn%C3%A9e][Section 5.3 du site web]]).

   Détailler (et justifier) votre protocole d'évaluation : qui a mesuré quoi, à combien de reprises, etc.
   
   Il n'est pas utile d'inclure 11 figures pour les 11 variables ; présenter et discuter un ou deux cas pertinents sera suffisant. En revanche, on peut éventuellement présenter des résumés pour les 11 variables (valeur du coefficient de concordance, etc.) dans une table.
3. Prendre ensuite le fichier TPS de l'un des étudiants du groupe comme fichier de référence pour la suite des analyses.
4. ACP sur l'ensemble des mesures ; commenter les différences observées.
5. À partir de l'ACP, identifier une ou deux variables intéressantes pour distinguer les artistes, et analyser ces variables isolément (voir [[https://f-santos.gitlab.io/tp-master-1-bgs/analyses-univari%C3%A9es-et-bivari%C3%A9es.html#analyses-univari%C3%A9es][Section 3.1 du cours]]), ou en les croisant (voir [[https://f-santos.gitlab.io/tp-master-1-bgs/analyses-univari%C3%A9es-et-bivari%C3%A9es.html#analyses-bivari%C3%A9es][Section 3.2 du cours]]). Alternativement, vous pouvez aussi analyser le ratio entre ces deux variables.
6. Discussion globale pour résumer les différences inter-artistes, etc.

** Quelques conseils supplémentaires (en vrac)
- Inspirez-vous des blocs de code présents sur le site du cours, qui sont réutilisables pratiquement à l'identique (en modifiant seulement quelques détails ou quelques arguments de fonctions à chaque fois). On n'attend pas nécessairement de vous d'innover par rapport à ce qui a été vu en TP, mais plutôt de réutiliser à bon escient la démarche qui y a été suivie, en l'adaptant et l'enrichissant si besoin.
- Pour consulter l'aide des fonctions utilisées sur le site du cours (et donc pour savoir la signification de chaque argument, et comment les modifier), rappelons que la fonction =help()= affiche la documentation de la fonction spécifiée. Par exemple, =help(PCA)= affiche l'aide de la fonction =PCA()= dédiée aux analyses en composantes principales.

* Quelques indications pour la conception du script R
** Packages à utiliser
Votre script pourra commencer par importer tous les packages requis pour vos analyses. Par exemple, les packages suivants seront sans doute nécessaires :
#+begin_src R :results output :session *R* :exports code
## Importer les packages :
library(epiR)
library(FactoMineR)
library(factoextra)
library(geomorph)
library(tidyverse)
#+end_src

#+RESULTS:

Vous pouvez en utiliser d'autres (y compris, bien sûr, des packages non abordés durant le TP !) selon vos besoins et les analyses que vous prévoyez.

** Fonction annexe
Toutes les opérations effectuées dans le [[https://f-santos.gitlab.io/tp-master-1-bgs/donn%C3%A9es.html][Chapitre 2 du cours]] ont été résumées en une unique fonction pour faciliter l'étape de mise en forme des données à partir de vos fichiers TPS. Vous pourrez par exemple importer vos données à l'aide de l'instruction suivante (à placer et exécuter elle aussi au début de votre script) :
#+begin_src R :results output :session *R* :exports both
source("https://page.hn/u7j4gw") # charge la fonction d'import
#+end_src

#+RESULTS:

Attention : un accès Internet sera requis.

** Importer les données
Toujours en supposant que votre groupe comporte deux étudiants, vous pouvez importer vos fichiers TPS en exécutant (et en adaptant) les instructions suivantes :
#+begin_src R :results output :session *R* :exports code :cache no
## Importer les données de l'étudiant Jack (série 1) :
jack1 <- load_m1asa(
    file = "./data/figures_jack1.TPS",  # nom du fichier TPS
    obs.name = "Jack1"                  # nom de l'observateur
)

## Importer les données de l'étudiant Jack (série 2) :
jack2 <- load_m1asa(
    file = "./data/figures_jack2.TPS",
    obs.name = "Jack2"
)

## Importer les données de l'étudiant Oggy :
oggy <- load_m1asa(
    file = "./data/figures_oggy.TPS",
    obs.name = "Oggy"
)
#+end_src

#+RESULTS[651c5d77543e2900a960aed2e71938567130d9a9]:
: 
: No curves detected; all points appear to be fixed landmarks.
: 
: No curves detected; all points appear to be fixed landmarks.
: 
: No curves detected; all points appear to be fixed landmarks.

#+begin_src R :results output :session *R* :exports both
## Visualiser (pour vérif) les premières lignes du dataframe jack1 :
head(jack1)
#+end_src

#+RESULTS:
#+begin_example
                 M1       M2       M3        M4       M5        M6       M7       M8
Aline_1_04 52.51482 32.22451 20.27892  8.685460 7.359140  4.395767 15.91890 21.49448
Aline_1_07 66.57680 42.48362 25.11151 11.935817 5.923876  6.520552 16.31976 25.57522
Aline_1_08 82.24744 45.24966 28.43591 17.802017 8.152022 10.306828 16.86953 22.41739
Aline_1_11 57.94145 32.97543 25.10246  8.736661 5.146615  6.368066 10.58363 18.61432
Aline_1_13 46.46686 29.84091 19.47860  5.987669 6.249350  7.248318 10.62451 15.05039
Aline_2_02 55.59274 36.02557 18.85457 13.052384 8.140481  3.220802 11.25961 19.17531
                  M9      M10       M11 artiste   obs
Aline_1_04  9.275950 2.803094 11.759869   Aline Jack1
Aline_1_07 13.156495 1.424808  9.733729   Aline Jack1
Aline_1_08 11.332186 4.235603 11.912120   Aline Jack1
Aline_1_11  9.423701 2.340854 11.188975   Aline Jack1
Aline_1_13  8.381203 1.762697  6.527746   Aline Jack1
Aline_2_02 12.540589 2.540686  8.982306   Aline Jack1
#+end_example

#+begin_src R :results output :session *R* :exports both
## Visualiser (pour vérif) les premières lignes du dataframe jack2 :
head(jack2)
#+end_src

#+RESULTS:
#+begin_example
                 M1       M2       M3        M4       M5        M6       M7       M8
Aline_1_04 52.59403 32.47253 20.35399  8.573940 7.396028  4.249979 15.96993 21.46784
Aline_1_07 66.76853 42.49435 25.32302 11.906102 6.378776  6.638221 14.39205 25.84659
Aline_1_08 82.46652 45.31170 28.32980 17.977261 8.259679 10.489759 17.12811 22.66286
Aline_1_11 58.11921 32.96169 25.39814  8.886434 5.134040  6.277333 10.83705 18.77295
Aline_1_13 46.68859 29.92687 19.48901  6.122370 6.346320  7.430430 11.03289 15.20497
Aline_2_02 55.69218 36.04465 18.81353 12.726845 8.205941  3.656487 12.43493 19.35984
                  M9      M10       M11 artiste   obs
Aline_1_04  9.239186 3.001669 11.905645   Aline Jack2
Aline_1_07 14.979341 1.386628  9.234974   Aline Jack2
Aline_1_08 11.356995 4.220155 11.936852   Aline Jack2
Aline_1_11  8.557958 2.319181 12.269591   Aline Jack2
Aline_1_13  8.270696 1.669034  6.614265   Aline Jack2
Aline_2_02 10.369367 2.680487 10.754808   Aline Jack2
#+end_example

#+begin_src R :results output :session *R* :exports both
## Visualiser (pour vérif) les premières lignes du dataframe oggy :
head(oggy)
#+end_src

#+RESULTS:
#+begin_example
                 M1       M2       M3        M4       M5        M6       M7       M8
Aline_1_04 52.26717 32.39337 20.15057  8.522584 7.390497  4.195239 16.06164 21.31793
Aline_1_07 66.68037 42.44483 25.23831 12.227399 6.230116  6.633672 16.26108 25.87603
Aline_1_08 82.61638 45.38037 28.34305 18.224536 8.348933 10.206526 16.99125 22.69019
Aline_1_11 58.09246 33.02977 25.40029  9.158089 5.097846  6.430462 10.94832 18.69693
Aline_1_13 46.60857 29.91485 19.41262  8.184587 6.213996  7.246008 11.16682 15.17680
Aline_2_02 55.53390 36.28586 18.36282 13.082198 8.371969  3.338264 11.90164 19.32720
                  M9      M10       M11 artiste  obs
Aline_1_04  9.254416 2.863425 11.645861   Aline Oggy
Aline_1_07 13.378528 1.279431  9.815336   Aline Oggy
Aline_1_08 11.513865 4.202129 12.053646   Aline Oggy
Aline_1_11  8.917975 2.176290 11.525721   Aline Oggy
Aline_1_13  8.199546 1.720401  6.790703   Aline Oggy
Aline_2_02 11.673221 2.637609  9.743336   Aline Oggy
#+end_example

Remarques :
- pour un groupe de trois étudiants, rajouter simplement un troisième bloc de code similaire ;
- on suppose ici que vos fichiers de données sont initialement nommés =figures_jack1.TPS=, =figures_jack2.TPS= et =figures_oggy.TPS=, et sont contenus dans un sous-répertoire =data= de votre répertoire de travail, comme suggéré en Section [[Structurer votre répertoire de travail]]. Adaptez le code si besoin.

#+begin_export latex
\medskip
#+end_export
Pensez ensuite à effectuer quelques contrôles d'importation de vos données pour vérifier que tout est cohérent ; voir à cet effet [[https://f-santos.gitlab.io/tp-master-1-bgs/donn%C3%A9es.html#import-et-contr%C3%B4le-des-donn%C3%A9es-tps][la Section 2.2.2 du site web]]. (Par exemple, utilisez les fonctions =class()=, =dim()=, =print()=, =head()= ou encore =summary()= sur chacun des objets =jack1=, =jack2= et =oggy=. Le résultat de ces commandes n'a pas vocation à figurer dans votre rapport, mais doit vous permettre de vérifier l'absence de soucis d'importation.)

** Préparer les données pour l'analyse de l'erreur inter-observateurs
Il se /peut/, en fonction du design que vous aurez choisi, que tous les étudiants du groupe n'aient pas mesuré les mêmes figures, ni le même nombre de figures. C'est le cas ici :
#+begin_src R :results output :session *R* :exports both
## Figures mesurées par Jack lors de sa session 1 :
rownames(jack1)
#+end_src

#+RESULTS:
#+begin_example
 [1] "Aline_1_04"    "Aline_1_07"    "Aline_1_08"    "Aline_1_11"    "Aline_1_13"   
 [6] "Aline_2_02"    "Aline_2_03"    "Aline_2_08"    "Aline_2_13"    "Armance_1_02" 
[11] "Armance_1_05"  "Armance_1_06"  "Armance_1_07"  "Armance_1_10"  "Armance_1_12" 
[16] "Armance_2_01"  "Armance_2_02"  "Armance_2_03"  "Armance_2_06"  "Armance_2_07" 
[21] "Armance_2_08"  "Armance_2_09"  "Bruno_1_04"    "Bruno_1_09"    "Bruno_1_10"   
[26] "Bruno_2_04"    "Bruno_2_06"    "Bruno_2_11"    "Carole_2_04"   "Carole_2_08"  
[31] "Carole_2_09"   "Carole_2_13"   "Carole_2_14"   "Evi_1_01"      "Evi_1_02"     
[36] "Evi_1_04"      "Evi_1_06"      "Evi_1_10"      "Evi_2_01"      "Evi_2_03"     
[41] "Evi_2_07"      "Evi_2_09"      "Evi_2_11"      "Evi_2_12"      "Evi_2_13"     
[46] "Megane_1_01"   "Megane_1_04"   "Megane_2_07"   "Valentin_1_01" "Valentin_1_04"
[51] "Valentin_3_04"
#+end_example

#+begin_src R :results output :session *R* :exports both
## Figures mesurées par Oggy :
rownames(oggy)
#+end_src

#+RESULTS:
#+begin_example
 [1] "Aline_1_04"    "Aline_1_07"    "Aline_1_08"    "Aline_1_11"    "Aline_1_13"   
 [6] "Aline_2_02"    "Aline_2_03"    "Aline_2_08"    "Aline_2_13"    "Armance_1_02" 
[11] "Armance_1_05"  "Armance_1_06"  "Armance_1_07"  "Armance_1_10"  "Armance_1_12" 
[16] "Armance_2_01"  "Armance_2_02"  "Armance_2_03"  "Armance_2_06"  "Armance_2_07" 
[21] "Armance_2_08"  "Armance_2_09"  "Bruno_1_04"    "Bruno_1_09"    "Bruno_1_10"   
[26] "Bruno_2_04"    "Bruno_2_06"    "Bruno_2_11"    "Carole_2_04"   "Carole_2_08"  
[31] "Carole_2_09"   "Carole_2_13"   "Carole_2_14"   "Evi_1_01"      "Evi_1_02"     
[36] "Evi_1_04"      "Evi_1_10"      "Evi_2_01"      "Evi_2_03"      "Evi_2_07"     
[41] "Evi_2_09"      "Evi_2_11"      "Evi_2_12"      "Megane_1_01"   "Megane_1_04"  
[46] "Megane_2_07"   "Valentin_1_01" "Valentin_1_04" "Valentin_2_05" "Valentin_3_04"
#+end_example

Pour l'étude inter-observateurs entre Jack et Oggy, comme en [[https://f-santos.gitlab.io/tp-master-1-bgs/%C3%A9valuation-des-erreurs-de-mesure.html#pratique-avec-r-2][Section 5.3.2 du cours]], il est alors nécessaire de se restreindre aux figures mesurées par les deux étudiants :
#+begin_src R :results output :session *R* :exports both
## Figures communes entre les deux étudiants :
common <- intersect(rownames(jack1), rownames(oggy))

## Ramener les deux tableaux aux figures communes seulement :
inter1 <- jack1[common, ]
inter2 <- oggy[common, ]
#+end_src

#+RESULTS:

Cela crée deux nouveaux dataframes =inter1= et =inter2= que vous pouvez utiliser le temps de votre étude de l'erreur inter-observateurs, comme ci-dessous (voir Fig. [[fig-m10]]) :
#+begin_src R :results graphics file :file M10.png :exports both :width 400 :height 400 :session *R*
## Concordance Jack/Oggy pour la mesure M10 :
plot(
    x = inter1$M10, # 1ère série de mesures
    y = inter2$M10, # 2nde série de mesures
    pch = 16,       # figuré choisi pour les points
    xlab = "Jack",  # label de l'axe X
    ylab = "Oggy",  # label de l'axe Y
    main = "M10"    # titre principal
)
abline(c(0, 1))     # ajout de la droite de parfait accord
#+end_src

#+NAME: fig-m10
#+CAPTION: Nuage de points des mesures effectuées par les deux observateurs pour la variable M10. (Avec deux très jolies valeurs complètement merdouillées, au passage.)
#+ATTR_LATEX: :width 0.45 \textwidth
#+RESULTS:
[[file:M10.png]]

#+begin_src R :results output :session *R* :exports both
## Calcul du coef de concordance :
epi.ccc(x = inter1$M10, y = inter2$M10)$rho.c   
#+end_src

#+RESULTS:
: Erreur dans epi.ccc(x = inter1$M10, y = inter2$M10) : 
:   impossible de trouver la fonction "epi.ccc"

À vous d'adapter ce code à votre cas, et à d'autres variables.

** Suite des analyses
Pour tout le reste des analyses, vous pouvez utiliser les données d'un seul étudiant du groupe ; par exemple, utiliser uniquement le dataframe =jack1= pour tout le reste du rapport.
